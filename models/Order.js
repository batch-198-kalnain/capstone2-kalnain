const mongoose = require("mongoose");
const orderSchema = new mongoose.Schema({
	totalAmount:{
		type:Number,
		required:[true,"Amount is required"]
	},
	purchasedOn:{
		type:Date,
		default:new Date()
	},
	userId:{
		type:String,
		required:[true,"UserId is required"]
	},
	products:[
		{
			productId:{
				type:String,
				required:[true,"ProductId is required"]
			},
			quantity:{
				type:Number,
				default:1
			}
		}
	]
})

module.exports = mongoose.model("Order",orderSchema);