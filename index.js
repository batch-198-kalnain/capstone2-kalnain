// require ExpressJS and Mongoose
const express = require("express");
const mongoose = require("mongoose");

// Connect to MongoDB
mongoose.connect("mongodb+srv://admin:admin123@cluster0.aqugixu.mongodb.net/capstoneEcommerce?retryWrites=true&w=majority",{
	useNewUrlParser:true,
	useUnifiedTopology:true
});

// checking and logging the connection status of MongoDB
let db = mongoose.connection;
db.on('error',console.error.bind(console,"MongoDB Connection Error"));
db.once('open',()=>console.log("Connected to MongoDB"));

// assigning the port number and express app
const app = express();
const port = 4000;

// using the express json parser method.
app.use(express.json());

const userRoutes = require("./routes/userRoutes");
app.use('/users',userRoutes);

const productRoutes = require("./routes/productRoutes");
app.use('/products',productRoutes);

const orderRoutes = require("./routes/orderRoutes");
app.use('/orders',orderRoutes);

app.listen(port,()=> console.log("Server is running at port 4000!"));