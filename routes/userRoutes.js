const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");
const auth = require("../auth");
const {verify,verifyAdmin} = auth;

// Registration Route
router.post("/",userControllers.userRegistration);

// Login Route
router.post("/login",userControllers.userLogin);

// setAdmin Route
router.put("/setAdmin/:userId",verify,verifyAdmin,userControllers.setAdmin);

// getUserDetails Route
router.get("/getUserDetails",verify,userControllers.getUserDetails);
module.exports = router;