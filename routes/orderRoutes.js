const express = require("express");
const router = express.Router();

const orderControllers = require("../controllers/orderControllers");
const auth = require("../auth");
const {verify,verifyAdmin} = auth;

// Create order Route
router.post("/",verify,verifyAdmin,orderControllers.createOrders);

// getUserOrders Route
router.get("/getUserOrders",verify,orderControllers.getUserOrders);

// retrieveAllOrders Route
router.get("/",verify,verifyAdmin,orderControllers.getAllOrders);

// Display product per order Route
router.get("/productPerOrder/:orderId",verify,orderControllers.productPerOrder);

module.exports = router;