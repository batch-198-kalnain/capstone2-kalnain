const express = require("express");
const router = express.Router();

const productControllers = require("../controllers/productControllers");
const auth = require("../auth");
const {verify,verifyAdmin} = auth;

// Insert New Product Route
router.post("/insertProduct",verify,verifyAdmin,productControllers.insertNewProduct);

// Get All Active Products Route
router.get("/activeProducts",productControllers.getActiveProduct);

// Retrieve Single Product Route
router.get("/getProduct/:productId",productControllers.getSingleProduct);

// updateProduct Route
router.put("/updateProduct/:productId",verify,verifyAdmin,productControllers.updateProduct);

// archiveProduct Route
router.delete("/archiveProduct/:productId",verify,verifyAdmin,productControllers.archiveProduct);

// Activate Product Route
router.put("/activateProduct/:productId",verify,verifyAdmin,productControllers.activateProduct);

module.exports = router;