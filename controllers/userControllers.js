// importing the user model and bcrypt package
const User = require("../models/User");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");

// importing the authentication
const auth = require("../auth");

// module for user registration
module.exports.userRegistration = (req,res) =>{
	User.findOne({email:req.body.email})
	.then(foundEmail => {
		if(foundEmail === null){
			const hashedPass = bcrypt.hashSync(req.body.password,10);
			let newUserAccount = new User({
				firstName: req.body.firstName,
				lastName: req.body.lastName,
				mobileNo: req.body.mobileNo,
				email: req.body.email,
				password: hashedPass
			});

			newUserAccount.save()
			.then(result => res.send(result))
			.catch(error => res.send(error))
		} else {
			return res.send({message:"Email is already registered."})
		}
	})
};

module.exports.userLogin = (req,res) => {
	User.findOne({email:req.body.email})
	.then(foundUser => {
		if(foundUser === null) {
			return res.send({message:"No user found."})
		} else {
			const isPassword = bcrypt.compareSync(req.body.password,foundUser.password);
			if(isPassword){
				return res.send({accessToken: auth.createAccessToken(foundUser)});
			} else {
				return res.send({message:"Incorrect Password"})
			}
		}
	})
};

module.exports.setAdmin = (req,res) => {
	User.findById(req.params.userId)
	.then(foundUser => {
		if(foundUser === null) {
			return res.send({message:"No user found"});
		} else {
			foundUser.isAdmin = true;
			foundUser.save()
			.then(result => res.send(result))
			.catch(error => res.send(error))
		}
	})
};

module.exports.getUserDetails = (req,res) => {
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
};