// importing the user model and bcrypt package
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");

// importing the authentication
const auth = require("../auth");

module.exports.insertNewProduct = (req,res) => {
    let newProduct = new Product({
        name:req.body.name,
        description:req.body.description,
        price:req.body.price
    });

    newProduct.save()
    .then(result => res.send(result))
    .catch(error => res.send(error))
};

module.exports.getActiveProduct = (req,res) => {
    Product.find({isActive:true})
    .then(result => res.send(result))
    .catch(error => res.send(error))    
};

module.exports.getSingleProduct = (req,res) => {
    Product.findById(req.params.productId)
    .then(result => res.send(result))
    .catch(error => res.send({message:"No product found."}))
}

module.exports.updateProduct = (req,res) => {
    let updatedProduct = {
        name:req.body.name,
        description:req.body.description,
        price:req.body.price
    }
    Product.findByIdAndUpdate(req.params.productId,updatedProduct,{new:true})
    .then(result => res.send(result))
    .catch(error => res.send(error))
};

module.exports.archiveProduct = (req,res) => {
    let updatedProduct = {
        isActive:false
    }
    Product.findByIdAndUpdate(req.params.productId,updatedProduct,{new:true})
    .then(result => res.send(result))
    .catch(error => res.send(error))
};

module.exports.activateProduct = (req,res) => {
    let updatedProduct ={
        isActive:true
    }
    Product.findByIdAndUpdate(req.params.productId,updatedProduct,{new:true})
    .then(result => res.send(result))
    .catch(error => res.send(error))
};