// importing the user model and bcrypt package
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");

// importing the authentication
const auth = require("../auth");

module.exports.createOrders = (req,res) => {
    if(req.user.isAdmin === true) {
        return res.send({message:"Action Forbidden"})
    } else {
        let newOrder = new Order ({
            totalAmount:req.body.totalAmount,
            userId:req.user.id,
            products:req.body.products,
        });
        newOrder.save()
        .then(result => res.send({message:"Order Successful"}))
        .catch(error => res.send(error))
    }
};

module.exports.getUserOrders = (req,res) => {
    if(req.user.isAdmin === true){
        return res.send({message:"Action Forbidden"})
    } else {
        Order.find({userId:req.user.id})
        .then(result => res.send(result))
        .catch(error => res.send(error))
    }
};

module.exports.getAllOrders = (req,res) => {
    Order.find()
    .then(result => res.send(result))
    .catch(error => res.send(error))
};

module.exports.productPerOrder = (req,res) => {
    if(req.user.isAdmin === true) {
        return res.send({message:"Action Forbidden"})
    } else {
        Order.find({userId:req.user.id,_id:req.params.orderId})
        .then(result => res.send(result))
        .catch(error => res.send({message:"No order found."}))
    }
};